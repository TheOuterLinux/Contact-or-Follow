# Contact-or-Follow
Links and info to various social sites or ways of getting into contact.
However, not all are what I would consider "active" and may more or less either be place holders or just simply abandoned. Think of this as more or less of a "manifesto" of successes and failures.

* <b>Website:</b> [https://theouterlinux.gitlab.io](https://theouterlinux.gitlab.io)
* <b>Email (Encrypted email):</b> [theouterlinux@protonmail.com](mailto:theouterlinux@protonmail.com)
* <s>Patreon (Donate): https://patreon.com/theouterlinux</s>
* <b>Ko-fi (Donate):</b> https://ko-fi.com/theouterlinux
* <b>Locals (Patreon alternative?):</b> [https://linux.locals.com/](https://linux.locals.com/)
* <s>Twitter (Article dump): https://twitter.com/theouterlinux</s>
* <b>Mastodon (FOSS Twitter alternative):</b> [https://mastodon.social/@TheOuterLinux](https://mastodon.social/@TheOuterLinux)
* <b>PixelFed (FOSS Instagram alternative):</b> [https://pixelfed.social/TheOuterLinux](https://pixelfed.social/TheOuterLinux)
* <s>Tumblr (Command-line bookmarking): http://theouterlinux.tumblr.com/</s>
* <s>YouTube (Playlists): https://www.youtube.com/channel/UCK5qGhtF5XS3ngbqyQ7iBqw</s>
* <b>BitChute (Videos):</b> [https://bitchute.com/theouterlinux](https://bitchute.com/theouterlinux)
* <b>Vimeo (Videos):</b> [https://vimeo.com/theouterlinux](https://vimeo.com/theouterlinux)
* <b>Reddit (Forum):</b> [https://www.reddit.com/r/TheOuterLinux](https://www.reddit.com/r/TheOuterLinux)
* <b>SoundCloud (Music):</b> [https://soundcloud.com/theouterlinux](https://soundcloud.com/theouterlinux)
* <s>Twitch (Live): https://www.twitch.tv/theouterlinux</s>
* <b>Picarto (Live):</b> [https://www.picarto.tv/theouterlinux](https://www.picarto.tv/theouterlinux)
* <b>DeviantArt (Art):</b> [https://theouterlinux.deviantart.com/](https://theouterlinux.deviantart.com/)
* <s>ArtStation (Art): https://theouterlinux.artstation.com</s>
* <b>Sketchfab (3D models):</b> [https://www.sketchfab.com/theouterlinux](https://www.sketchfab.com/theouterlinux)
* <b>OpenGameArt (Video game assets):</b> [https://opengameart.org/users/theouterlinux](https://opengameart.org/users/theouterlinux)
* <b>Teepublic (Shirts, mugs, etc.):</b> [https://www.teepublic.com/user/theouterlinux](https://www.teepublic.com/user/theouterlinux)
* <s>Inprnt (Buy prints): https://www.inprnt.com/profile/theouterlinux/</s>
* <b>Archive.org:</b> [https://archive.org/details/@theouterlinux](https://archive.org/details/@theouterlinux)
* <b>Slashdot (Rantings):</b> [https://slashdot.org/~TheOuterLinux](https://slashdot.org/~TheOuterLinux)
* <b>LinuxQuestions (Hmmm?):</b> [https://www.linuxquestions.org/questions/blog/theouterlinux-1169710/](https://www.linuxquestions.org/questions/blog/theouterlinux-1169710/)
* <s>Disquis: [https://disqus.com/by/theouterlinux/](https://disqus.com/by/theouterlinux/)</s>
* <b>Imgur</b>: [https://imgur.com/user/TheOuterLinux](https://imgur.com/user/TheOuterLinux)
* <s>Community.Unix.com: [https://community.unix.com/u/theouterlinux](https://community.unix.com/u/theouterlinux)</s>
* <b>Itch.io (Games):</b> [https://theouterlinux.itch.io/](https://theouterlinux.itch.io/)
---
# [Projects](https://theouterlinux.gitlab.io/Projects/Projects.html)
><i>The following is a list of note-worthy projects TheOuterLinux has either created, is associated with, owns, maintains, blah blah blah. Feel free to visit the main website's [projects page](https://theouterlinux.gitlab.io/Projects/Projects.html) for a longer list. Most of these projects have an associated video (See [PeerTube](https://peertube.mastodon.host/video-channels/theouterlinux_channel) or [BitChute](https://bitchute.com/theouterlinux) channels).</i>

### PsychOS

PsychOS is currently a 32-bit, i686, Devuan-based GNU/Linux distribution aimed at retrophiles. The purpose of the project is to support older and lighter hardware for as long as possible. And even though there is only an i686 version, there are plans to create an i486 (RAM might be a problem) and a Free-DOS based version. 

Please don't complain about not having an isohybrid. I want you to try it, but I could care less whether or not PsychOS runs "a bit funky" on your new UEFI computer. You should not have thrown away your "old trusty." Worse case scenario, just use VirtualBox of QEMU. You may have to choose "Failsafe" mode when asked.

><i>Before E-Mailing or Bug reporting in relation to the PsychOS project, please take the time to read through the [docs](https://psychoslinux.gitlab.io/documents/docs.html), including the ones found in the system's ~/LookHere folder.</i>

* <b>Website:</b> [https://psychoslinux.gitlab.io](https://psychoslinux.gitlab.io)
* <b>Email (Encrypted email):</b> [psychosgnulinux@protonmail.com](mailto:psychosgnulinux@protonmail.com)
* <b>Bug Reporting:</b> [https://gitlab.com/PsychOSLinux/psychoslinux.gitlab.io/issues](https://gitlab.com/PsychOSLinux/psychoslinux.gitlab.io/issues)
* <b>Forum:</b> [https://reddit.com/r/psychoslinux](https://reddit.com/r/psychoslinux)

### StreamPi

>This is included with PsychOS.

I got tired of OBS Studio not being nice to older or lighter hardware, so I created StreamPi for people to use to live stream and/or locally record with. It is essentially a GUI front-end, created with Gambas, for ffmpeg. But before I did that there was/is also a command-line version.

### RetroGrab

>This is not listed on the main site's Project page but is included with PsychOS.

Have you ever wondered what it would be like to have a package manager but for older software and various other resources and be launched from your normal applications menu once installed? That's basically what RetroGrab is, except instead of hosting the software myself, it checks against two key encrypted files for URL's and other information and runs the scripts necessary to make it happen. It is sort of like a download manager with bookmarks that I can change at any time.

### TheOuterLinux Kodi Addon

Yeah, I have a Kodi addon. It's not much, but it the idea is to have an easy way for people to see most of my art/media related creations. It also includes links to various other things I find cool. But what makes this interesting is that I figured out a way to change most of the displayed information at any time instead of forcing people to have to update the addon itself. It's licensed as GPLv3, so have at it.

### Img2QB, Img2BBC, and Img2VDU

I like most BASIC languages, but dealing with graphics for most of them is a pain. So, I made a few Python scripts to convert modern image formats to DATA arrays or draws them using customized ASCII characters (BBC BASIC).

![Follow me on Mastodon](https://img.shields.io/mastodon/follow/243183.svg?style=popout-square&logo=mastodon)
![Follow me on Reddit](https://img.shields.io/reddit/subreddit-subscribers/theouterlinux.svg?style=popout-square&logo=reddit)
![Discord](https://img.shields.io/discord/271061594358677517.svg?style=popout-square&logo=discord)
![Twitch](http://streambadge.com/twitch/dark/theouterlinux.png)
![Picarto](https://api.picarto.tv/v1/channel/name/TheOuterLinux/status.png)
---
